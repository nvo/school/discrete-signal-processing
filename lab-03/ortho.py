from numpy import *
# import matplotlib
# matplotlib.use('qt4agg')
import matplotlib.pyplot as plt

f2 = exp(1j*2*pi*2*arange(10)/10)
f5 = exp(1j*2*pi*5*arange(10)/10)
f25 = f2*f5
for i in range(10):
    plt.plot([0, real(f25[i])], [0, imag(f25[i])], 'b')
    plt.axis([-1, 1, -1, 1])
    plt.gca().set_aspect('equal', 'box')
    plt.draw()
    plt.pause(1)
plt.show()
