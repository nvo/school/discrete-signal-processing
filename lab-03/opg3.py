from random import random
from scipy.fftpack import fft
import matplotlib.pyplot as plt

N = 1024
x = [random() for _ in range(N)]
X = abs(fft(x))

plt.plot(X)
plt.show()

x = [random() - 0.5 for _ in range(N)]
X = abs(fft(x))

plt.plot(X)
plt.show()
