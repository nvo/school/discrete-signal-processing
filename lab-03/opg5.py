import pickle
import numpy as np
from scipy.fftpack import fft, fftfreq
import matplotlib.pyplot as plt


def coswav(frequency: float, sample_frequency: float, duration: int):
    length = sample_frequency * duration
    step = 2 * np.pi * frequency / sample_frequency
    return np.cos(np.arange(0, length * step, step))


f = open('toon.pickle', 'rb')
z = pickle.load(f, encoding='latin1')
f.close()

fs = 8000
t = 1

plt.plot(z[:100])
plt.show()

Z = abs(fftfreq(fs * t, z))
plt.plot(Z)
plt.show()

for _ in np.argsort(Z)[:-10:-1]:
    print(_)

# TODO: recreate z with cos-waves
