import numpy as np
from scipy.fftpack import fft, fftfreq
import matplotlib.pyplot as plt


def coswav(frequency: float, sample_frequency: float, duration: int):
    length = sample_frequency * duration
    step = 2 * np.pi * frequency / sample_frequency
    return np.cos(np.arange(0, length * step, step))


f = 400
fs = 8000
t = 500
x = coswav(f, fs, t)
X = abs(fft(x))
print(np.argmax(X))

plt.plot(X)
plt.show()

X = abs(fftfreq(fs * t, x))
plt.plot(X)
plt.show()
