from numpy import *
#import matplotlib
#matplotlib.use('qt4agg')
import matplotlib.pyplot as plt

s1=cos(arange(0,2*pi,2*pi/360))
s2=sin(arange(0,2*pi,2*pi/360))

plt.plot(s1,label='cos')
plt.plot(s2,label='sin')
plt.plot(-s1,label='-cos')
plt.plot(-s2,label='-sin')
plt.plot(sqrt(2)*s1/2+sqrt(2)*s2/2,label='cos+sin (genormaliseerd)')
plt.plot(-sqrt(2)*s1/2-sqrt(2)*s2/2,label='-cos-sin (genormaliseerd)')
plt.xlim([0,360])
plt.ylim([-1.2,2.8])
plt.legend()
plt.show()


