import numpy as np
import numpy.typing as npt
from scipy.fftpack import fft
import matplotlib.pyplot as plt


def coswav(f: int, fs: int, length: float):
    n = fs * length
    step = 2 * np.pi * f / fs
    return np.cos(np.arange(0, n * step, step))


def spectrogram(x, fs):
    plt.figure()
    plt.specgram(x, NFFT=1024, Fs=fs, noverlap=512)
    plt.show()


f = 10
fs = 500
ms = 0.5

x = coswav(f, fs, ms)
X = abs(fft(x))
plt.plot(x)
plt.show()
plt.plot(X)
plt.show()
spectrogram(x, fs)

z = x[:-30]
Z = abs(fft(z))
plt.plot(z)
plt.show()
plt.plot(Z)
plt.show()
spectrogram(z, fs)
