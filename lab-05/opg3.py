import numpy as np
import numpy.typing as npt
from scipy.fftpack import fft, ifft, fftshift
from scipy.signal import convolve, firwin
from scipy.signal.windows import gaussian
from scipy.io import wavfile as wav
import matplotlib.pyplot as plt


def coswav(f: int, fs: int, length: float):
    n = fs * length
    step = 2 * np.pi * f / fs
    return np.cos(np.arange(0, n * step, step))


def spectrogram(x, fs):
    plt.figure()
    plt.specgram(x, NFFT=1024, Fs=fs, noverlap=512)
    plt.show()


f = 10
fs = 500
ms = 0.5

x = coswav(f, fs, ms)
X = abs(fft(x))
# plt.plot(x)
# plt.show()
# plt.plot(X)
# plt.show()
# spectrogram(x, fs)

z = x[:-30]
Z = abs(fft(z))
# plt.plot(z)
# plt.show()
# plt.plot(Z)
# plt.show()
# spectrogram(z, fs)

std = 25
w = gaussian(len(z), std)
y = z * w
Y = abs(fft(y))
# plt.plot(y)
# plt.show()
# plt.plot(Y)
# plt.show()
# spectrogram(y, fs)


N = 40

w = firwin(N, fs / 4, fs=fs)
plt.plot(w)
plt.show()
w_pad = np.concatenate(
    [
        np.zeros(int(N * 9 / 2)),
        w,
        np.zeros(int(N * 9 / 2))
    ]
)
plt.plot(w)
plt.plot(w_pad)
plt.show()
W = abs(fft(w))
W_pad = abs(fft(w_pad))
plt.plot(W)
plt.plot(W_pad)
plt.show()
