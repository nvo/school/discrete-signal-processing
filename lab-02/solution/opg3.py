import numpy as np
from scipy.io import wavfile
from scipy.signal import decimate, resample
# import matplotlib
# matplotlib.use('Qt4Agg')
import matplotlib.pyplot as plt


def coswav(f, fs, duur):
    lengte = fs * duur
    stap = 2 * np.pi * f / fs
    return np.cos(np.arange(0, lengte * stap, stap))


def wavwrite(filename, fs, signaal):
    normalized = np.int16(signaal / max(np.fabs(signaal)) * 32767)
    wavfile.write(filename, fs, normalized)


def spectrogram(signaal, fs):
    plt.figure()
    plt.specgram(signaal, NFFT=1024, Fs=fs, noverlap=512)
    plt.show()


def plot_signal(signals):
    plt.figure(figsize=(10, 2))

    for signal in signals:
        indices = np.arange(0, len(signal), 1000)
        signal2 = signal[indices]

        plt.axis([0, len(signal), -2, 2])
        plt.plot(signal)
        plt.plot(indices, signal2, marker='o', linestyle='None')
    plt.show()


def downsample(signal, fs_orig, fs_new):
    return signal[::fs_orig//fs_new]


def upsample(signal, fs_orig, fs_new):
    y = np.array([])
    for x in signal:
        y = np.append(y, [x])
        y = np.append(y, np.repeat(0, fs_new // fs_orig))
    return y


def upsample2(signal, fs_orig, fs_new):
    y = np.array([])
    for a, b in zip(signal[:-1], signal[1:]):
        y = np.append(y, [a])

        n = fs_new // fs_orig
        for i in range(n):
            y = np.append(y, a + i * (b - a) / n)
    return y


# F = [3000, 7000]
# fs = 32000

# y = coswav(F[0], fs, .25)
# for f in F[1:]:
#     y += coswav(f, fs, .25)

# wavwrite('3k7k.wav', fs, y)
# spectrogram(y, fs)

# fs_new = 8000
# y_ds = downsample(y, fs, fs_new)
# wavwrite('3k7k_ds.wav', fs_new, y_ds)
# spectrogram(y_ds, fs_new)

# y_aa = decimate(y, fs // fs_new)
# wavwrite('3k7k_aa.wav', fs_new, y_aa)
# spectrogram(y_ds, fs_new)


# Upsampling
f = 1000
fs = 8000
fs_new = 32000
y = coswav(f, fs, .25)
# y_us = upsample(y, fs, fs_new)
# plot_signal([y_us[:50]])

# wavwrite('1k_orig.wav', fs, y)
# wavwrite('1k_us.wav', fs_new, y_us)

y_us2 = upsample2(y, fs, fs_new)
y_us3 = resample(y, fs_new // fs * len(y))
plot_signal([y_us2[:50], y_us3[:50]])
