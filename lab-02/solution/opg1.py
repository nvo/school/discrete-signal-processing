import numpy as np
# import matplotlib
# matplotlib.use('Qt4Agg')
import matplotlib.pyplot as plt


def coswav(f, fs, duur):
    lengte = fs * duur
    stap = 2 * np.pi * f / fs
    return np.cos(np.arange(0, lengte * stap, stap))


def plot_wavs(F, fs, duur):
    plt.figure(figsize=(10, 2))
    for i, f in enumerate(F):
        toon = coswav(f, fs, duur)
        indices = np.arange(0, len(toon), 1000)
        toon2 = toon[indices]

        plt.axis([0, len(toon), -2, 2])
        plt.plot(toon)
        if i == 0:
            plt.plot(indices, toon2, marker='o', linestyle='None')
    plt.show()


F = [(95, 5, 195), (75, 25, 175), (55, 45, 155), (50, 150)]
fs = 100000

for f in F:
    plot_wavs(f, fs, .25)
