import numpy as np
from scipy.io import wavfile


def coswav(f, fs, duur):
    lengte = fs * duur
    stap = 2 * np.pi * f / fs
    return np.cos(np.arange(0, lengte * stap, stap))


def wavwrite(filename, fs, signaal):
    normalized = np.int16(signaal / max(np.fabs(signaal)) * 32767)
    wavfile.write(filename, fs, normalized)


F = [440, 1000, 3000, 5000, 7000]

fs = 8000
for f in F:
    toon = coswav(f, fs, 2)
    wavwrite('{}.wav'.format(f), fs, toon)
