from math import *
from scipy import *
from scipy.io import wavfile

def coswav(f,fs,duur):
	lengte=fs*duur
	stap=2*pi*f/fs
	return cos(arange(0,lengte*stap,stap))
	
def wavwrite(filename,fs,signaal):
	normalized=int16(signaal/max(fabs(signaal))*32767)
	wavfile.write(filename,fs,normalized)
	
fs=8000
toon=coswav(440,fs,2)
wavwrite('440.wav',fs,toon)
