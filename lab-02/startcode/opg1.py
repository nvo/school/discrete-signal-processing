from math import *
from scipy import *
#import matplotlib
#matplotlib.use('Qt4Agg')
import matplotlib.pyplot as plt

def coswav(f,fs,duur):
	lengte=fs*duur
	stap=2*pi*f/fs
	return cos(arange(0,lengte*stap,stap))
	
toon=coswav(100,100000,.25)
indices=arange(0,len(toon),1000)
toon2=toon[indices]

plt.figure(figsize=(10,2))
plt.axis([0,len(toon),-2,2])
plt.plot(toon,color='r')
plt.plot(indices,toon2,marker='o',linestyle='None')
#bemerk de 2 verschillende syntaxen van plot: ofwel geef je enkel de y-lijst mee, ofwel geef je een x-lijst en een y-lijst mee
#in dit geval is de x-lijst van toon2 nodig om op de juiste plaats over het andere signaal te plotten
plt.show()

