from math import *
from scipy import *
from scipy.io import wavfile
#import matplotlib
#matplotlib.use('Qt4Agg')
import matplotlib.pyplot as plt

def coswav(f,fs,duur):
	lengte=fs*duur
	stap=2*pi*f/fs
	return cos(arange(0,lengte*stap,stap))
	
def wavwrite(filename,fs,signaal):
	normalized=int16(signaal/max(fabs(signaal))*32767)
	wavfile.write(filename,fs,normalized)
	
def spectrogram(signaal,fs):
	plt.figure()
	plt.specgram(signaal,NFFT=1024,Fs=fs,noverlap=512)
	plt.show()
	

