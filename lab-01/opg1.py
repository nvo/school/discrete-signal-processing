import matplotlib.pyplot as plt
from numpy import *
import cmath
import math


def cart2pol(a_real, b_im):  # convert from cartesian to polar coordinates
    z = complex(a_real, b_im)
    phase = cmath.phase(z)
    mod = abs(z)
    print('The modulus of the complex number z=%s+%sj is: %s' %
          (str(round(a_real, 2)), str(round(b_im, 2)), str(round(mod, 4))))
    print('The phase of the complex number z=%s+%sj is: %s' %
          (str(round(a_real, 2)), str(round(b_im, 2)), str(round(phase, 4))))
    mod_phase = [mod, phase]
    return mod_phase


def pol2cart(mod, phase):  # convert from polar to cartesian coordinates
    a_real = mod * (math.cos(phase))
    b_im = mod * (math.sin(phase))
    print('The real part of the complex number z=%se^{%sj} is: %s' % (
        str(round(mod, 2)), str(round(phase, 2)), str(round(a_real, 4))))
    print('The imaginary part of the complex number z=%se^{%sj} is: %s' % (
        str(round(mod, 2)), str(round(phase, 2)), str(round(b_im, 4))))
    real_im = [a_real, b_im]
    return real_im


# plot a complex number and its inverse, conjugate,and conjugate inverse
def plot_complex_numbers(z):
    c_numbers = array([z, 1/z, conj(z), conj(1/z)])
    fig = plt.figure()
    plt.axes().set_aspect('equal')
    # plot unit circle
    t = linspace(0, 2*pi, 101)
    plt.plot(cos(t), sin(t))
    # adjust plot box to largest number, at least showing the unit circle
    m = max(max(abs(c_numbers.real)), max(abs(c_numbers.imag)), 1.1)
    plt.xlim(-1.1*m, 1.1*m), plt.ylim(-1.1*m, 1.1*m)
    # draw real and im axis
    plt.plot([0, 0], [-1.1*m, 1.1*m], 'k', linewidth=0.5)
    plt.plot([-1.1*m, 1.1*m], [0, 0], 'k', linewidth=0.5)
    plt.grid()
    # plot data
    plt.plot(c_numbers.real, c_numbers.imag, 'ro')
    # annotate plot with labels
    a_txt = str(round(real(z), 2))
    b_txt = str(round(imag(z), 2))
    plt.text(z.real+0.1, z.imag, '$z=%s+%sj$' % (a_txt, b_txt), fontsize=12)
    plt.text((1/z).real+0.1, (1/z).imag, '$1/z$', fontsize=12)
    plt.text(conj(z).real+0.1, conj(z).imag, '$z^*$', fontsize=12)
    plt.text(conj(1/z).real+0.1, conj(1/z).imag, '$1/z^*$', fontsize=12)
    plt.ylabel('Imaginary')
    plt.xlabel('Real')


# define real and imaginary part of the complex number
a_real = 2  # sqrt(2)/4;
b_im = 3  # sqrt(2)/8;
# calculates the modulus and phase (in rad) of the complex number in cartesian coordinates
[mod, phase] = cart2pol(a_real, b_im)

# define modulus and phase of the complex number
mod = 1
phase = pi / 4
# calculates the real and imaginary part of the complex number in polar coordinates
[a_real2, b_im2] = pol2cart(mod, phase)

# plot related complex numbers for a given input
z = complex(a_real, b_im)
plot_complex_numbers(z)


# z     = 2 + 3j
# r     = sqrt(2**2 + 3**2) = sqrt(13) = 3.606
# theta = atan(3 / 2) = 0.9828
# => z = 3.606 * exp(0.9828j)

# z*    = 2 - 3j
# r     = sqrt(2**2 + (-3)**2) = sqrt(13) = 3.606
# theta = atan(-3 / 2) = -0.9828
# => z = 3.606 * exp(-0.9828j)
