from math import *
from scipy import *
import numpy as np
from scipy.io import wavfile
import matplotlib.pyplot as plt

# f:    freq van het signaal
# fs:   geeft aan uit hoeveel samples per seconde je signaal bestaat
# duur: de tijdsduur van het signaal in seconden


def coswav(f, fs, duur):
    lengte = fs*duur
    stap = 2*pi*f/fs
    return np.cos(np.arange(0, lengte * stap, stap))


def spectrogram(signaal, fs):
    plt.figure()
    plt.specgram(signaal, NFFT=1024, Fs=fs, noverlap=512)
    plt.show()

# wegschrijven wavwrite


def wavwrite(filename, fs, signaal):
    normalized = np.int16(signaal/np.max(np.fabs(signaal))*32767)
    wavfile.write(filename, fs, normalized)


# 3. Spectogram

fs = 32000
d = 1

f0 = 400
f1 = 2000

x0 = coswav(f0, fs, d)
x1 = coswav(f1, fs, d)
x2 = x0 + x1
chirp_fs, chirp = wavfile.read("chirp1.wav")

wavwrite('x0.wav', fs, x0)
wavwrite('x1.wav', fs, x1)
wavwrite('x2.wav', fs, x0)

spectrogram(x0, fs)
spectrogram(x1, fs)
spectrogram(x2, fs)
spectrogram(chirp, chirp_fs)

N = 800
N = 2000
plt.figure(figsize=(10, 2))
plt.plot(x0[:N], color='r')
plt.plot(x1[:N], color='b')
plt.plot(x2[:N], color='m')
plt.plot(chirp[:N], color='k')
plt.show()

# 4. Convolutie
x = [0, 0, 0, 0, 0, 0, 1, 2, 3, 2, 1, 0]
h = [1/3, 1/3, 1/3]

y = np.convolve(x, h)
print(y)
