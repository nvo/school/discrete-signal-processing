# Impulse Response

import numpy as np
import numpy.typing as npt
from scipy.fftpack import fft
import matplotlib.pyplot as plt


def impulse(n: int, length: int) -> npt.ArrayLike:
    f = np.zeros(length)
    f[n] = 1
    return f


N = 500
o = N >> 1

d = impulse(o, N)
D = abs(fft(d))

plt.plot(d)
plt.show()

plt.plot(D)
plt.show()
