import numpy as np
import numpy.typing as npt
from scipy.fftpack import fft, ifft
from scipy.signal import convolve
from scipy.signal.windows import gaussian
import matplotlib.pyplot as plt


def impulse(
    impulse_start: int,
    impulse_length: int,
    strength: float,
    length: int
) -> npt.ArrayLike:
    f = np.zeros(length)
    f[impulse_start:impulse_start + impulse_length] = strength
    return f


def coswav(f: int, fs: int, length: int):
    n = fs * length
    step = 2 * np.pi * f / fs
    return np.cos(np.arange(0, n * step, step))


f1 = 200
f2 = 3700
fs = 8000
l = 1
x = coswav(f1, fs, l) + 2 * coswav(f2, fs, l)
X = fft(x)

plt.plot(abs(X))
plt.show()

l = len(X)
A = impulse(0, 2000, 1, l) + impulse((l >> 1) + 2000, 2000, 1, l)
plt.plot(A)
plt.show()

Y = X * A
y = np.real(ifft(Y))

C = len(x) >> 1
N = 25
plt.plot(y[C-N:C+N])
plt.show()
