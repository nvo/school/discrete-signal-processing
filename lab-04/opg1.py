import numpy as np
import numpy.typing as npt
from scipy.fftpack import fft
from scipy.signal import convolve
import matplotlib.pyplot as plt


def impulse(
    impulse_start: int,
    impulse_length: int,
    strength: float,
    length: int
) -> npt.ArrayLike:
    f = np.zeros(length)
    f[impulse_start:impulse_start + impulse_length] = strength
    return f


def coswav(f: int, fs: int, length: int):
    n = fs * length
    step = 2 * np.pi * f / fs
    return np.cos(np.arange(0, n * step, step))


f1 = 200
f2 = 3700
fs = 8000
l = 1
x = coswav(f1, fs, l) + 2 * coswav(f2, fs, l)

c = len(x) >> 1
N = 25
plt.plot(x[c-N:c+N])
plt.show()

l = 100
t = 10
a = impulse((l - t) >> 1, t, 1. / t, l)

y = convolve(x, a)
plt.plot(y[c-N:c+N])
plt.show()

plt.plot(abs(fft(a)))
plt.show()
