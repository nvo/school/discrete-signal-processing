import numpy as np
import numpy.typing as npt
from scipy.fftpack import fft
from scipy.signal import convolve
from scipy.signal.windows import gaussian
import matplotlib.pyplot as plt


def impulse(
    impulse_start: int,
    impulse_length: int,
    strength: float,
    length: int
) -> npt.ArrayLike:
    f = np.zeros(length)
    f[impulse_start:impulse_start + impulse_length] = strength
    return f


def coswav(f: int, fs: int, length: int):
    n = fs * length
    step = 2 * np.pi * f / fs
    return np.cos(np.arange(0, n * step, step))


f1 = 200
f2 = 3700
fs = 8000
l = 1
x = coswav(f1, fs, l) + 2 * coswav(f2, fs, l)

C = len(x) >> 1
N = 25
plt.plot(x[C-N:C+N])
plt.show()

l = 100
c = 3
a = gaussian(l, c)
plt.plot(a)
plt.show()

y = convolve(x, a)
plt.plot(y[C-N:C+N])
plt.show()

plt.plot(abs(fft(a)))
plt.show()
