import numpy as np
import numpy.typing as npt
from scipy.fftpack import fft, ifft, fftshift
from scipy.signal import convolve
from scipy.signal.windows import gaussian
from scipy.io import wavfile as wav
import matplotlib.pyplot as plt


def impulse(
    impulse_start: int,
    impulse_length: int,
    on_strength: float,
    off_strength: float,
    length: int
) -> npt.ArrayLike:
    f = np.ones(length) * off_strength
    f[impulse_start:impulse_start + impulse_length] = on_strength
    return f


def coswav(f: int, fs: int, length: int):
    n = fs * length
    step = 2 * np.pi * f / fs
    return np.cos(np.arange(0, n * step, step))


def spectrogram(x, fs):
    plt.figure()
    plt.specgram(x, NFFT=1024, Fs=fs, noverlap=512)
    plt.show()


# TODO: filter is not working
fs, x = wav.read('BBC_corrupted.wav')
X = fft(x)
spectrogram(x[:2*fs], fs)
plt.plot(X)
plt.show()
print(np.argmax(X))

N = 4000
noise = 330
std = 10
idx = noise - std >> 1
# H = impulse(idx, std, 0, 1, N) + \
#     impulse(-idx - std, std, -1, 0, N)

# H = np.ones(N)
# H[idx:idx + std] = 0
# H[-idx - std:-idx] = 0

H = np.ones(N) / N
H[idx:idx + std] = 0
H[-idx - std:-idx] = 0

plt.plot(H)
plt.show()

h = np.real(ifft(H))
h = fftshift(h)
y = convolve(x, h)
spectrogram(y[:2*fs], fs)
plt.plot(y[:fs])
plt.show()

wav.write('BBC.wav', fs, y)
